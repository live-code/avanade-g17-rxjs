import { HttpEvent, HttpHandler, HttpInterceptor, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError, delay, finalize, first, map, mergeMap, tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthService
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log(req);
    return this.authService.getToken$()
      .pipe(
        first(),
        mergeMap(token => {
          let cloned = req;
          if (token) {
            cloned = req.clone({
              headers: req.headers.set('Authentication', token)
            });
          }
          return next.handle(cloned)
            .pipe(
              delay(environment.production ? 0 : 1000),
              catchError(err => {
                switch (err.status) {
                  case 401:
                    this.authService.logout();
                    break;
                  default:
                  case 404:
                    this.authService.logout();
                    break;
                }
                return throwError(null);
              })
            );
        })
      );
  }

}

/*

let cloned = req;

if (this.authService.ifLogged && req.url.includes('localhost') ) {
  cloned = req.clone({
    params,
    headers: req.headers.set('Authentication', this.authService.token)
  });
}
return next.handle(cloned)
*/

// http client
// -----o--o---xo-----------oooo----->

// form
// ---ooooo-----o-----o----o-------->

// router
// ----o-----o-----o--o-o--o------->
