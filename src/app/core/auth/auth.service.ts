import { Injectable } from '@angular/core';
import { Auth } from './auth';
import { BehaviorSubject, interval, Observable, of, Subject } from 'rxjs';
import { map, share, shareReplay } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  auth$ = new BehaviorSubject<Auth>(null);
  timerObs$ = new BehaviorSubject<number>(0)

  constructor(private http: HttpClient, private router: Router) {
    interval(1000).subscribe(this.timerObs$)
  }

  login(username: string = 'mario', password: string = '123'): Observable<Auth> {
    const params = new HttpParams()
      .set('username', username)
      .set('params', password);

    const req$ = this.http.get<Auth>(`http://localhost:3000/login`, { params })
      .pipe(shareReplay(1))

    req$.subscribe(
      res => {
        this.auth$.next( res );
        localStorage.setItem('auth', JSON.stringify(res))
        this.router.navigateByUrl('admin');
      },
    );
    return req$;

  }

  logout(): void {
    this.auth$.next(null);
    this.router.navigateByUrl('login')
  }


  ifLogged$(): Observable<boolean> {
    return this.auth$
      .pipe(
        map(obj => !!obj)
      );
  }

  getDisplayName$(): Observable<string> {
    return this.auth$
      .pipe(
        map(obj => obj?.displayName)
      );
  }

  getToken$(): Observable<string> {
    return this.auth$
      .pipe(
        map(obj => obj?.token)
      );
  }
}
