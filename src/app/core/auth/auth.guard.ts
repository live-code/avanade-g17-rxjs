import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { delay, map, switchMap, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root'})
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
    private http: HttpClient
  ) {}

  // sync check authentication
  canActivate2(): Observable<boolean> {
    return this.authService.ifLogged$()
      .pipe(
        tap(isLogged => {
          if (!isLogged) {
            this.router.navigateByUrl('login');
          }
        })
      );
  }

  // async
  canActivate3(): Observable<boolean> {
    return this.http.get<{ response: string }>('http://localhost:3000/validate')
      .pipe(
        delay(1000),
        map(res => res.response === 'ok')
      );
  }

  // async
  canActivate(): Observable<boolean> {
    return this.authService.getToken$()
      .pipe(
        switchMap(token => this.http.get<{ response: string }>(`http://localhost:3000/validate?token=${token}`)),
        map(res => res.response === 'ok')
      );
  }


}
