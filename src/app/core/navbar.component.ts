import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth/auth.service';
import { Auth } from './auth/auth';

@Component({
  selector: 'ava-navbar',
  template: `

    <button routerLink="login">login</button>
    <button routerLink="admin">admin</button>
    <button routerLink="settings">settings</button>

    <!-- {{authService.auth?.displayName}}-->
    {{(authService.getDisplayName$() | async)}}

  `,
  styles: [
  ]
})
export class NavbarComponent  {

  constructor(public authService: AuthService) {}

}
