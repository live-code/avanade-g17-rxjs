import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ava-root',
  template: `
    <ava-navbar></ava-navbar>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
  title = 'angular-g17-rxjs';

  constructor(private router: Router) {

  }
}
