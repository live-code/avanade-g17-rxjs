import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth/auth.service';

@Component({
  selector: 'ava-login',
  template: `
    <h1>login</h1>
    <input type="text">
    <input type="text">
    <button (click)="loginHandler()">Login</button>
  `,
  styles: [
  ]
})
export class LoginComponent {

  constructor(public authService: AuthService) {

  }

  loginHandler(): void {
    this.authService.login()
      .subscribe(val => {
        console.log(val)
        alert('logged')
      })
  }

}
