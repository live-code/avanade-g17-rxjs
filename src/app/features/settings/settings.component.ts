import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { AuthService } from '../../core/auth/auth.service';
import { interval, Subscription } from 'rxjs';
import { FormControl } from '@angular/forms';
import { debounceTime, pairwise, shareReplay, switchMap, withLatestFrom } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'ava-settings',
  template: `
    
    <input type="text" [formControl]="input">
    
    <p>
      {{authService.timerObs$ | async}}
      {{(authService.getDisplayName$() | async)}}
    </p>
    
    <button (click)="connect()">Connect to Timer</button>
  `,
  styles: [
  ]
})
export class SettingsComponent {
  @Output() doSomething = new EventEmitter()
  input = new FormControl();
  timer$ = interval(1000)
    .pipe(
      shareReplay(1),
      pairwise()
    );

  constructor(public authService: AuthService, private http: HttpClient) {
    this.input.valueChanges
      .pipe(
        debounceTime(1000),
        withLatestFrom(this.authService.timerObs$),
        switchMap(values => this.http.get(`http://localhost:3000/fake?text=${values[0]}interval=${values[1]}`))
      )
      .subscribe(res => {
        console.log(res)
      })

    this.timer$.subscribe()
  }

  connect(): void {
    this.timer$.subscribe(val => console.log('B', val))
  }
}
